import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) {
  }

  public getInfo(query: string) {
    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQCtiF1FfPpSNJjcc1GMtjrRJEorVMom2L-roGjnHRrdeNjqcTfnduFy-gAsbkLbZdNhe2DpKqChvp_Kj6s'
    });

    return this.http.get(url, { headers });

  }

  public getNewReleases() {

    return this.getInfo('browse/new-releases?limit=48')
     .pipe( map( data => data['albums'].items));
  }

  public getArtists(finish: string) {

    return this.getInfo(`search?q=${ finish }&type=artist&limit=30`)
      .pipe( map( data => data['artists'].items));
  }

  public getArtist(id: string) {

    return this.getInfo(`artists/${id}`);
  }
}
