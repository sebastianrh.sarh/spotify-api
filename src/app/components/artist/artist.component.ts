import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent {

  artist: any = {};
  loading: boolean;

  constructor(
    private router: ActivatedRoute,
    private spotify: SpotifyService) { 
      this.loading = true;
    this.router.params.subscribe( params =>{
      this.getArtist(params['id']);
    })
  }

  public getArtist(id:string) {
    this.loading = true;
    this.spotify.getArtist(id).subscribe(data =>{
      console.log(data);
      this.artist = data;
      this.loading = false;
    })
  }
}
