import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  newReleases: any[] = [];
  loading: boolean;

  constructor(
    private spotify: SpotifyService,
    private router: Router
    ) {

    this.loading = true;

    this.spotify.getNewReleases().subscribe((data: any) => {
      this.newReleases = data;
      this.loading = false;
    });
  }

  public showArtist(newRelease: any) {
    let artistId;
    if (newRelease.type === 'artist') {
      artistId = newRelease.id;
    } else {
      artistId = newRelease.artists[0].id;
    }
    this.router.navigate(['/artist', artistId]);
  }
}
