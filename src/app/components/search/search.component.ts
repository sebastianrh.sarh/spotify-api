import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  artists: any[] = [];
  loading: boolean;

  constructor(
    private spotify: SpotifyService,
    private router: Router
    ) { }

  public search(finish: string) {
    this.loading = true;
    this.spotify.getArtists(finish).subscribe((data: any) => {
      this.artists = data;
      this.loading = false;
    });
  }

  public showArtist(artist: any) {
    let artistId;
    if (artist.type === 'artist') {
      artistId = artist.id;
    } else {
      artistId = artist.artists[0].id;
    }
    this.router.navigate(['/artist', artistId]);
  }
}
